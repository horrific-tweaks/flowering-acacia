This resource pack adds a variant of acacia leaves that have orange flowers, much like the 1.17 flowering azalea trees.

![A screenshot of a flowering acacia tree.](./.images/acacia.png)
